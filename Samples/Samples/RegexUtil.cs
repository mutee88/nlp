﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Samples
{
    public class RegexUtil
    {
        public static bool TryMatch(string pattern, string input, out int matchCount)
        {
            input = Regex.Replace(input, "[^a-zA-Z0-9 ]+", "");//remove all non alphanumeric  characters
            pattern = "(^| )(" + pattern + ")( |$)";
            Match match = Regex.Match(input, pattern, RegexOptions.IgnoreCase);

            if (match.Success)
            {
                matchCount = match.Groups.Count;
                return true;
            }
            else
            {
                matchCount = 0;
                return false;
            }
        }
    }
}

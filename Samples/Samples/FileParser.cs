﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Samples
{
    public class EmailTitles
    {
        public EmailTitles()
        {
            this.Inbox = new List<string>();
            this.Spam = new List<string>();
        }
        public List<string> Inbox{get;set;}
        public List<string> Spam{get;set;}
    }

    public enum LineState
    {
        INBOX,
        SPAM
    }
    public class FileParser
    {
        public static EmailTitles ReadFile()
        {
            var titles = new EmailTitles();
            using (var rd = new StreamReader(@"..\..\titles.txt"))
            {
                bool skip = true;
                LineState state = LineState.INBOX;
                while (rd.EndOfStream == false)
                {
                    string line = rd.ReadLine();
                    if (line.ToLower() == "inbox corpus")
                    {
                        skip = true;
                        state = LineState.INBOX;
                    }
                    else if (line.ToLower() == "spam corpus")
                    {
                        skip = true;
                        state = LineState.SPAM;
                    }
                    else
                    {
                        skip = false;
                    }

                    if (!skip)
                    {
                        if (state == LineState.INBOX)
                            titles.Inbox.Add(line.Trim());
                        else
                            titles.Spam.Add(line.Trim());
                    }
                }
                return titles;
            }
        }
    }
}

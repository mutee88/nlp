﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Samples
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "Sam is hungry and Dan is hungry as well";
            string pattern = "(is hungry)";
            Match match = Regex.Match(text, pattern, RegexOptions.IgnoreCase);

            if (match.Success)
                Console.WriteLine("OK");
            //RunApp();

            Console.ReadKey();
        }

        static void RunApp()
        {
            while (true)
            {
                Console.WriteLine("enter a phrase");
                string s = Console.ReadLine();
                List<string> ngrams = NGramGenerator.Generate(2, s);
                double prodProbInbox = 1F, prodProbSpam = 1F;
                EmailTitles titles = FileParser.ReadFile();


                //inbox titles                              
                for (int x = 0; x < ngrams.Count; x++)
                {
                    string gram = ngrams[x];
                    int globalCount = 0;
                    int leftCount = 0;
                    double currentGramInboxProb = 0.0, currentGramSpamProb = 0.0;
                    foreach (var title in titles.Inbox)
                    {
                        int matchCount;
                        bool globalMatch = RegexUtil.TryMatch(gram, title, out matchCount);
                        if (globalMatch && matchCount==1)
                            globalCount++;
                        else if (globalMatch && matchCount > 1)
                            globalCount += matchCount;

                        matchCount = 0;
                        bool leftMatch = RegexUtil.TryMatch(gram.Split()[0], title, out matchCount);
                        if (leftMatch && matchCount == 1)
                            leftCount++;
                        else if (leftMatch && matchCount > 1)
                            leftCount += matchCount;
                    }

                    if (leftCount == 0)
                        currentGramInboxProb = 0;
                    else
                        currentGramInboxProb += (double)globalCount / (double)leftCount;
                    globalCount = 0;
                    leftCount = 0;

                    foreach (var title in titles.Spam)
                    {
                        int matchCount;
                        bool globalMatch = RegexUtil.TryMatch(gram, title, out matchCount);
                        if (globalMatch && matchCount == 1)
                            globalCount++;
                        else if (globalMatch && matchCount > 1)
                            globalCount += matchCount;

                        matchCount = 0;
                        bool leftMatch = RegexUtil.TryMatch(gram.Split()[0], title, out matchCount);
                        if (leftMatch && matchCount == 1)
                            leftCount++;
                        else if (leftMatch && matchCount > 1)
                            leftCount += matchCount;
                    }
                    if (leftCount == 0)
                        currentGramSpamProb = 0.0;
                    else
                        currentGramSpamProb += (double)globalCount / (double)leftCount;

                    prodProbInbox *= currentGramInboxProb;
                    prodProbSpam *= currentGramSpamProb;
                }

                Console.WriteLine("inbox {0:0.00}", prodProbInbox);
                Console.WriteLine("spam {0:0.00}", prodProbSpam);

                string leaning;

                if (prodProbInbox > prodProbSpam)
                {
                    leaning = "Inbox";
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                else if (prodProbSpam == prodProbInbox)
                {
                    leaning = "Non distinguishable. Hint reduce phrase length";
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                }
                else
                {
                    leaning = "Spam";
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                }

                Console.Write("Phrase likely to be in: ");
                Console.WriteLine(leaning);
                Console.WriteLine();
                Console.ResetColor();
            }
        }
    }
}

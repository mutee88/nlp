﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Samples
{
    public class NGramGenerator
    {
        /// <summary>
        /// tokenized based on spacing between words
        /// </summary>
        /// <param name="n"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> Generate(int n, string input)
        {
            string[] words = input.Split();
            List<string> ngrams = new List<string>();
            for (int k = 0; k < (words.Length - n); k++)
            {
                string s = "";
                int start = k;
                int end = k + n;
                for (int j = start; j < end; j++)
                {
                    s = s + " " + words[j].ToLower();
                }
                ngrams.Add(s.Trim());
            }
            ngrams.Add(words[words.Length - 2] + " " + words[words.Length - 1]);//the last batch
            return ngrams;
        }
    }
}
